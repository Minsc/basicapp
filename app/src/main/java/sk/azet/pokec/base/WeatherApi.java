package sk.azet.pokec.base;

import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by Roman Masarovič on 7/21/16.
 */

public class WeatherApi {
    private static String API_URL = "api.openweathermap.org/data/2.5/";
    public static String API_KEY = "3f3ee8409e7d1eecc7a52c8e527d99d2";
    private static WeatherApiInterface weatherApiInterface;
    private static String develop = "developer";
    private static String userAgent = "";
    private static Retrofit retrofit;

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public static WeatherApiInterface getApiService() {
        if (BuildConfig.BUILD_TYPE.equals("alfa")) {
            API_URL = "http://api.openweathermap.org/data/2.5/";
        }
        if (BuildConfig.BUILD_TYPE.equals("beta")) {
            API_URL = "http://api.openweathermap.org/data/2.5/";
        }
        if (BuildConfig.BUILD_TYPE.equals("debug")) {
            API_URL = "http://api.openweathermap.org/data/2.5/";
        }
        if (BuildConfig.BUILD_TYPE.equals("prerelease")) {
            API_URL = "http://api.openweathermap.org/data/2.5/";
        }
        if (BuildConfig.BUILD_TYPE.equals("release")) {
            API_URL = "http://api.openweathermap.org/data/2.5/";
        }
        if (weatherApiInterface == null) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(httpLoggingInterceptor)
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient).build();
            weatherApiInterface = retrofit.create(WeatherApiInterface.class);
        }
        return weatherApiInterface;
    }


    public interface WeatherApiInterface {
        @GET("forecast")
        Call<ForecastResult> getForecastResultCall(@QueryMap Map<String, String> map);
    }
}
