package sk.azet.pokec.base;

/**
 * Created by Roman Masarovič on 7/21/16.
 */

public class ForecastResult {
    int code;
    String message;
    City city;
}
