package sk.azet.pokec.base;


import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {


    public int maxTemperature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainFragment mainFragment = MainFragment.newInstance("","");
        mainFragment.setActivity(this);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,mainFragment)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public int getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(int maxTemperature) {
        this.maxTemperature = maxTemperature;
    }
}
