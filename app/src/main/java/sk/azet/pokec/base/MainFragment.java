package sk.azet.pokec.base;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private MainActivity activity;
    private TextView textView;

    public MainFragment() {
    }


    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View baseView = inflater.inflate(R.layout.fragment_main, container, false);
        textView = (TextView) baseView.findViewById(R.id.city);
        textView.postDelayed(new Runnable() {
            @Override
            public void run() {
                getForecast();
            }
        }, 2000);
        return baseView;
    }


    public void onButtonPressed() {
        Toast.makeText(getContext(), "make forecast request", Toast.LENGTH_SHORT).show();
    }


    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }

    private void setMaxTemperature() {
        ((MainActivity) getActivity()).setMaxTemperature(23);
    }

    private void getForecast() {
        final Gson gson = new Gson();
        ForecastContent content = new ForecastContent();
        content.setId(524901);
        content.setAppid(WeatherApi.API_KEY);
        Map map = ForecastContent.getForecastContent(content);
        final Call<ForecastResult> call = WeatherApi.getApiService().getForecastResultCall(map);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Response<ForecastResult> response = null;
                try {
                    response = call.execute();
                    final String text= response.body().city.name;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText(text);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ForecastResult forecastResult = response.body();
            };
        });
        thread.start();
    }
}
