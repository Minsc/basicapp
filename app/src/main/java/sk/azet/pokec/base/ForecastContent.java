package sk.azet.pokec.base;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Roman Masarovič on 7/21/16.
 */

public class ForecastContent {
    private int id;
    private String appid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public static Map getForecastContent(ForecastContent forecastContent) {
        Map<String, String> map = new HashMap<>();
        Gson gson = new Gson();
        int id = forecastContent.getId();
        String appid= forecastContent.getAppid();
        try {
            map.put("id", URLEncoder.encode(gson.toJson(id), "UTF-8"));
            map.put("appid", appid);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return map;
    }
}
